package com.example.practica03java;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnIngresar;
    private Button btnSalir;
    private EditText txtUsuario;
    private EditText txtContraseña;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingresar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        btnIngresar = findViewById(R.id.btnIng);
        btnSalir = findViewById(R.id.btnSalir);
        txtUsuario = findViewById(R.id.txtUsuario);
        txtContraseña = findViewById(R.id.txtContraseña);
    }

    private void ingresar() {
        String strUsuario;
        String strContraseña;

        strUsuario = getResources().getString(R.string.usuario);
        strContraseña = getResources().getString(R.string.contraseña);

        if (strUsuario.equals(txtUsuario.getText().toString()) && strContraseña.equals(txtContraseña.getText().toString())) {
            // Crear un Bundle para enviar información
            Bundle bundle = new Bundle();
            bundle.putString("usuario", txtUsuario.getText().toString());

            // Crear un intent para llamar a otra actividad
            Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
            intent.putExtras(bundle);

            // Iniciar la actividad esperando o no una respuesta
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Compruebe el usuario y/o contraseña", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Salir de la app");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Cancelar, no se realiza ninguna acción
            }
        });
        confirmar.show();
    }
}