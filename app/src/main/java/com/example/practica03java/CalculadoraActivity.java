package com.example.practica03java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMulti;
    private Button btnDiv;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblUsuario;
    private TextView lblResultado;
    private EditText txtUno;
    private EditText txtDos;

    // Declarar el objeto de la calculadora
    private Calculadora calculadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComponentes();
        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString("usuario");
        lblUsuario.setText(usuario);
        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSumar();
            }
        });
        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnRestar();
            }
        });
        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnMulti();
            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDiv();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLimpiar();
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnRegresar();
            }
        });
    }

    private void iniciarComponentes() {
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnMulti = findViewById(R.id.btnMultiplicar);
        btnDiv = findViewById(R.id.btnDividir);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblUsuario = findViewById(R.id.lblUsuario);
        lblResultado = findViewById(R.id.lblResultado);
        txtUno = findViewById(R.id.txtNum1);
        txtDos = findViewById(R.id.txtNum2);

        // Inicializar la calculadora
        calculadora = new Calculadora(0, 0);
    }

    public void btnSumar() {
        String num1Text = txtUno.getText().toString();
        String num2Text = txtDos.getText().toString();

        if (!num1Text.isEmpty() && !num2Text.isEmpty()) {
            calculadora.setNum1(Integer.parseInt(num1Text));
            calculadora.setNum2(Integer.parseInt(num2Text));

            int total = calculadora.suma();
            lblResultado.setText(String.valueOf(total));
        } else {
            Toast.makeText(getApplicationContext(), "Ingresa todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void btnRestar() {
        String num1Text = txtUno.getText().toString();
        String num2Text = txtDos.getText().toString();

        if (!num1Text.isEmpty() && !num2Text.isEmpty()) {
            calculadora.setNum1(Integer.parseInt(num1Text));
            calculadora.setNum2(Integer.parseInt(num2Text));

            int total = calculadora.resta();
            lblResultado.setText(String.valueOf(total));
        } else {
            Toast.makeText(getApplicationContext(), "Ingresa todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void btnMulti() {
        String num1Text = txtUno.getText().toString();
        String num2Text = txtDos.getText().toString();

        if (!num1Text.isEmpty() && !num2Text.isEmpty()) {
            calculadora.setNum1(Integer.parseInt(num1Text));
            calculadora.setNum2(Integer.parseInt(num2Text));

            int total = calculadora.multiplicacion();
            lblResultado.setText(String.valueOf(total));
        } else {
            Toast.makeText(getApplicationContext(), "Ingresa todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void btnDiv() {
        String num1Text = txtUno.getText().toString();
        String num2Text = txtDos.getText().toString();

        if (!num1Text.isEmpty() && !num2Text.isEmpty()) {
            calculadora.setNum1(Integer.parseInt(num1Text));
            calculadora.setNum2(Integer.parseInt(num2Text));

            int total = calculadora.division();
            lblResultado.setText(String.valueOf(total));
        } else {
            Toast.makeText(getApplicationContext(), "Ingresa todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void btnLimpiar() {
        lblResultado.setText("");
        txtUno.setText("");
        txtDos.setText("");
    }

    public void btnRegresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Regresar al MainActivity");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                // No hacer nada
            }
        });
        confirmar.show();
    }
}
